﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public class Rule : IRule
    {
        public delegate string ProcessMustacheEngineDelegate(string key, ref int indexStart, ref string template, JsonValue data);

        private Func<string, bool> _condition;
        private ProcessMustacheEngineDelegate _process;

        public Rule(Func<string, bool> condition, ProcessMustacheEngineDelegate process)
        {
            _condition = condition;
            _process = process;
        }

        public bool Condition(string key)
        {
            return _condition(key);
        }

        public string Process(string key, ref int indexStart, ref string template, JsonValue data)
        {
            return _process(key, ref indexStart, ref template, data);
        }
    }
}
