﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Engine renderEngine = Engine.GetInstance();

                string fileTemplate = "C:\\Work\\testovoie dankolab nsk xamarin\\MustacheEngine\\MustacheEngine\\Termplate.txt";
                string fileData = "C:\\Work\\testovoie dankolab nsk xamarin\\MustacheEngine\\MustacheEngine\\Data.txt";

                renderEngine.Render(fileTemplate, fileData);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
