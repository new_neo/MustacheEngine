﻿using System.Json;

namespace MustacheEngine
{
    public interface IRulesEngine
    {
        string Run(string key, ref int indexStart, ref string template, JsonValue data);
    }
}