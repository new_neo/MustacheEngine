﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public class Engine
    {
        private RulesEngine _rulesEngine;
        private static Engine _engine;
        private Engine()
        {
            _rulesEngine = new RulesEngine(
                new IRule[] {
                            RulesFactory.InstanceRuleForSharpe()
                            , RulesFactory.InstanceRuleForExclamation()
                            , RulesFactory.InstanceRuleForInvertedSections()
                            , RulesFactory.InstanceRuleForSlash()
                            , RulesFactory.InstanceRuleForBrace()
                            }
                , RulesFactory.InstanceRuleForEndRule()
                );
        }

        public static Engine GetInstance()
        {
            if(_engine == null)
            {
                _engine = new Engine();
            }
            return _engine;
        }
        
        public string Render(string fileTemplate, string fileData)
        {
            JsonValue data;
            using (FileStream fileStream = File.OpenRead(fileData))
            {
                data = JsonObject.Load(fileStream);
            }
            TemplateMustache template = TemplateMustache.InstanceFromFile(fileTemplate);

            if (data != null)
            {
                switch (data.JsonType)
                {
                    case JsonType.Object:
                        {
                            Console.WriteLine(  Render(
                                                    template
                                                    , data)
                                             );
                            break;
                        }
                    case JsonType.Array:
                        {
                            foreach(JsonValue item in data)
                            {
                                Console.WriteLine(  Render(
                                                    template
                                                    , item)
                                             );
                            }
                            break;
                        }
                }
            }
            return "";
        }

        public string Render(TemplateMustache templateMustache, JsonValue data)
        {
            string result="";
            string template = templateMustache.GetTemplate();
            int i = 0;
            int j;
            j = template.IndexOf("{{", i);

            while (j > -1)
            {
                                
                result += template.Substring(i, j - i);
                i = j+2;

                j = template.IndexOf("}}", i);
                string key = template.Substring(i, j - i);
                i = j + 2;
                
                result += _rulesEngine.Run(key, ref i, ref template, data);

                j = template.IndexOf("{{", i);
            }
            result += template.Substring(i);

            return result;
        }
    }
}
