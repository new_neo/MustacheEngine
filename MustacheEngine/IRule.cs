﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public interface IRule
    {
        bool Condition(string key);
        string Process(string key, ref int indexStart, ref string template, JsonValue data);
    }
}
