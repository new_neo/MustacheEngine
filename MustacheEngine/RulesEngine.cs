﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public class RulesEngine : IRulesEngine
    {
        private IEnumerable<IRule> _rules;
        private IRule _endRule;
        public RulesEngine(IEnumerable<IRule> rules, IRule endRule)
        {
            _rules = rules;
            _endRule = endRule;
        }

        public string Run(string key, ref int indexStart, ref string template, JsonValue data)
        {
            foreach (IRule rule in _rules)
            {
                if(rule.Condition(key))
                {
                    return rule.Process(key, ref indexStart, ref template, data);
                }
            }
            return _endRule.Process(key, ref indexStart, ref template, data);          
        }

    }
}
