﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public static class ToolsMustache
    {
        public static string GetEndTagFromKey(string key)
        {
            return string.Format("{{{{/{0}}}}}", key);
        }

        public static string GetInversedSectionTag(string key)
        {
            return string.Format("{{{{^{0}}}}}", key);
        }

    }
}
