﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MustacheEngine
{
    public class RulesFactory
    {
        public static IRule InstanceRuleForSharpe()
        {
            return new Rule(
                (key) => key.StartsWith("#")
                ,
                (string key, ref int indexStart, ref string template, JsonValue data) =>
                {
                    string result = "";
                    key = key.TrimStart('#');
                    string tagEnd = ToolsMustache.GetEndTagFromKey(key);
                    int j = template.IndexOf(tagEnd, indexStart);
                    if (j == -1)
                        throw new Exception(string.Format("Ошибка в определении параметра {0}, не найден закрывающий тег", key));

                    if (data[key] == null)
                    {
                        indexStart = j + tagEnd.Length;
                        return result;
                    }

                    switch (data[key].JsonType)
                    {
                        case JsonType.Boolean:
                            {
                                if ((bool)data[key])
                                    result += template.Substring(indexStart, j - indexStart);
                                indexStart = j + tagEnd.Length;
                                break;
                            }
                        case JsonType.Array:
                            {
                                if (data[key].Count != 0)
                                    foreach (JsonValue item in data[key])
                                        result += Engine.GetInstance().Render(
                                                                TemplateMustache.InstanceFromString(template.Substring(indexStart, j - indexStart))
                                                                , item);
                                indexStart = j + tagEnd.Length;
                                break;
                            }
                        case JsonType.Object:
                            {
                                if (data[key].Count != 0)
                                    result += Engine.GetInstance().Render(
                                                            TemplateMustache.InstanceFromString(template.Substring(indexStart, j - indexStart))
                                                            , data[key]);
                                indexStart = j + tagEnd.Length;
                                break;
                            }
                        default:
                            {
                                throw new Exception($"Неверный тип данных для параметра {key}");
                                break;
                            }
                    }
                    return result;
                }
                );
        }

        public static IRule InstanceRuleForExclamation()
        {
            return new Rule(
               (key) => key.StartsWith("!")
               ,
               (string key, ref int indexStart, ref string template, JsonValue data) =>
               {
                  return "";
               }
               );
        }

        public static IRule InstanceRuleForSlash()
        {
            return new Rule(
               (key) => key.StartsWith("/")
               ,
               (string key, ref int indexStart, ref string template, JsonValue data) =>
               {
                   throw new Exception(string.Format("Ошибка в определении параметра {0} в позиции {1}", key, indexStart));
               }
               );
        }

        public static IRule InstanceRuleForInvertedSections()
        {
            return new Rule(
               (key) => key.StartsWith("^")
               ,
               (string key, ref int indexStart, ref string template, JsonValue data) =>
               {
                   string result = "";
                   key = key.TrimStart('^');
                   string tagEnd = ToolsMustache.GetEndTagFromKey(key);
                   int j = template.IndexOf(tagEnd, indexStart);
                   if (j == -1)
                       throw new Exception(string.Format("Ошибка в определении параметра {0}, не найден закрывающий тег", key));

                   if (data[key] == null)
                   {
                       indexStart = j + tagEnd.Length;
                       return result;
                   }

                   switch (data[key].JsonType)
                   {
                       case JsonType.Boolean:
                           {
                               if (!(bool)data[key])
                                   result += template.Substring(indexStart, j - indexStart);
                               indexStart = j + tagEnd.Length;
                               break;
                           }
                       case JsonType.Array:
                           {
                               if (data[key].Count == 0)
                                   result += template.Substring(indexStart, j - indexStart);
                               indexStart = j + tagEnd.Length;
                               break;
                           }
                       case JsonType.Object:
                           {
                               if (data[key].Count == 0)
                                   result += template.Substring(indexStart, j - indexStart);
                               indexStart = j + tagEnd.Length;
                               break;
                           }
                       default:
                           {
                               //throw new Exception($"Неверный тип данных для параметра {key}");
                               break;
                           }
                   }
                   return result;
               }
               );
        }
        public static IRule InstanceRuleForEndRule()
        {
            return new Rule(
               (key) => true
               ,
               (string key, ref int indexStart, ref string template, JsonValue data) =>
               {
                   return HttpUtility.HtmlEncode( (data[key]?.ToString()??"").Trim('"') );
               }
               );
        }

        public static IRule InstanceRuleForBrace()
        {
            return new Rule(
               (key) => key.StartsWith("{")
               ,
               (string key, ref int indexStart, ref string template, JsonValue data) =>
               {
                   string result = "";
                   key = key.TrimStart('{');
                   string tagEnd = "}";
                   if (template[indexStart] != '}')
                       throw new Exception(string.Format("Ошибка в шаблоне, не найден закрывающий тег }}}}}} , {0}", indexStart));

                   indexStart++;
                   return (data[key]?.ToString() ?? "").Trim('"');
               }
               );
        }
    }
}
