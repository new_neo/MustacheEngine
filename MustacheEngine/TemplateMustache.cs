﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MustacheEngine
{
    public class TemplateMustache : ITemplate
    {
        private string _template;
        private string _fileTemplate;

        private TemplateMustache()
        {

        }
        private TemplateMustache(string fileTemplate)
        {
            _fileTemplate = fileTemplate;

            using (FileStream fileStream = File.OpenRead(fileTemplate))
            {
                StreamReader streamReader = new StreamReader(fileStream);
                _template = streamReader.ReadToEnd();
            }
        }

        

        public static TemplateMustache InstanceFromFile(string fileTemplate)
        {
            return new TemplateMustache(fileTemplate);
        }
        public static TemplateMustache InstanceFromString(string template)
        {
            return new TemplateMustache() {_template = template };
        }



        public string GetTemplate()
        {
            return _template;
        }
    }
}
